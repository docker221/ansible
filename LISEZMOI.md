# Conteuneur Ansible avec les outils nécessaires pour le déploiement (addon)

Vous trouverez ici la documentation en anglais [README.md => README.md](README.md).  

Avec ce conteneur, vous pouvez utiliser toute les lignes de commande Ansible:

* ansible
* ansible-config
* ansible-console
* ansible-doc
* ansible-galaxy
* ansible-inventory
* ansible-playbook
* ansible-pull
* ansible-vault

La commande par défaut du conteuneur est "ansible-playbook"  

Il y a une version de Python 2 (2.7.18) et Python 3 (mise à jour au moment du push de l'image).  
Même si Python 2 est abandonnée, certain vieux systèmes en on besoin pour la maintenance. Donc je garde une Python 2 au cas ou.  
Vous pouvez construire et utiliser le conteuneur derrière un proxy.  

## Que contient le conteneur

### Addon pour Ansible

La liste des addons Ansible par défaut pour Python 3 (dernière version d'Ansible) sont dans le fichier [collection-requirements.yml](collection-requirements.yml).

### Dépendances nécessaires pour Alpine

La liste des packets Alpine de dépendance des modules Ansible installées se trouve dans le fichier [Dockerfile.template](Dockerfile.template).

### Python dependencies

La liste des packets Python de dépendance des modules Ansible installées se trouve dans le fichier [Dockerfile.template](Dockerfile.template).

## Référence rapide

* Où poser des question et déclarer des bugs: [https://gitlab.com/docker221/ansible/-/issues](https://gitlab.com/docker221/ansible/-/issues).

## tag existant

Les tags suivent les versions d'Ansible, les derniers tag pour Python 2 et 3 sont:

* [9.3.0][def], [9.3.0-alpine3.19-py3.12.2][def]
* [2.3.3][def2], [2.3.3-alpine3.11-py2.7.18][def2]

Pour voir l'historiqu des tags, regarder [ici](https://hub.docker.com/r/vandman/ansible/tags)

## Comment utiliser le conteuneur

Lancement d'un Playbook Ansible "root-playbook.yml" et utilisation d'une connection en tant que root avec clé SSH. La commande suivante déploie la platforme PLD de l'inventaire.

```shell
docker run --rm \
  --mount type=bind,src=${HOME}/.ssh,dst=/root/.ssh \
  --mount type=bind,src=/dir/to/ansible-playbook,dst=/srv/ansible \
  --workdir /srv/ansible \
  --env ANSIBLE_FORCE_COLOR=true \
  --env ANSIBLE_VERBOSITY=1
  vandman/ansible \
    -e @vars/main.yml \
    -i inventories/PLD/hosts \
    root-playbook.yml;
```

Dans le paramètre --env de docker, vous pouvez utiliser tous les variables d'environnement par défaut d'Ansible. Vous trouverez ces variables dans la [documentation officiel Ansible](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#environment-variables).  
Le répertoire contenant le playbook est monté dans le conteneur dans **"/srv/ansible"**.  

Vous pouvez trouver d'autres exemples d'utilisation du conteneur dans le Makefile du projet [lancement_ansible](https://gitlab.com/t8384/lancement_ansible).

[def]: python3/Dockerfile
[def2]: python2/Dockerfile
