#!/usr/bin/env bash
#################################################
# Script to update the Docker Hub documentation #
#################################################
cp README.md docker-hub.md
sed -i "s|(LISEZMOI.md)|(https://gitlab.com/docker221/ansible/-/blob/main/LISEZMOI.md)|g" docker-hub.md
sed -i "s|(collection-requirements.yml)|(https://gitlab.com/docker221/ansible/-/blob/main/collection-requirements.yml)|g" docker-hub.md
sed -i "s|(Dockerfile.template)|(https://gitlab.com/docker221/ansible/-/blob/main/Dockerfile.template)|g" docker-hub.md
sed -i "s|python3/Dockerfile|https://gitlab.com/docker221/ansible/-/blob/main/python3/Dockerfile|g" docker-hub.md
sed -i "s|python2/Dockerfile|https://gitlab.com/docker221/ansible/-/blob/main/python2/Dockerfile|g" docker-hub.md
docker run --rm -t \
-v $(pwd):/myvol \
-v ${HOME}/.docker/config.json:/dockerhub/config.json \
--user $(id -u) \
-e PUSHRM_TARGET='vandman/ansible' \
-e PUSHRM_CONFIG='/dockerhub/config.json' \
chko/docker-pushrm:1 \
--file /myvol/docker-hub.md \
--short "Container to execute all ansible command line" \
--debug vandman/ansible
rm -f docker-hub.md
