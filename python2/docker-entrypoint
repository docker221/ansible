#!/bin/sh
set -eo pipefail

find "/docker-entrypoint.d/" -follow -type f -print | sort -V | while read -r f; do
  if [ "$(file -b --mime-type "$f")" == "text/x-shellscript" ]; then
    if [ -x "$f" ]; then
      echo "Launching $f";
      "$f"
    else
        # warn on shell scripts without exec bit
        echo "Ignoring $f, not executable"
    fi
  else
    echo "Ignoring $f, not a shell script"
  fi
done

echo "REQUIREMENTS_GALAXY=${REQUIREMENTS_GALAXY:-requirements.yml}"
echo "NO_GALAXY_REQUIREMENT=$NO_GALAXY_REQUIREMENT"
echo "REQUIREMENTS_PIP=${REQUIREMENTS_PIP:-requirements.txt}"
echo "NO_PIP_REQUIREMENT=$NO_PIP_REQUIREMENT"
if [ -f "${REQUIREMENTS_PIP:-requirements.txt}" ] && [ -z "$NO_PIP_REQUIREMENT" ]; then
  gosu pip install -r "${REQUIREMENTS_PIP:-requirements.txt}"
fi

if [ -f "${REQUIREMENTS_GALAXY:-requirements.yml}" ] && [ -z "$NO_GALAXY_REQUIREMENT" ]; then
  gosu ansible-galaxy install -r "${REQUIREMENTS_GALAXY:-requirements.yml}"
fi

if [ -n "$REGISTRY_LOGIN" ] && [ -n "$REGISTRY_PASSWORD" ] ; then
  echo $REGISTRY_PASSWORD | docker login --username $REGISTRY_LOGIN --password-stdin $REGISTRY_HOST
fi

# if no command, display version info
if [ "$#" -eq 0 ]; then
    set -- ansible-playbook --version
fi
# if command starts with an option, prepend ansible-playbook
if [ "${1:0:1}" = '-' ]; then
    set -- ansible-playbook "$@"
fi

exec "$@"
