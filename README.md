# Ansible container with addon tools for depoyement

Here is the French [README.md => LISEZMOI.md](LISEZMOI.md).  

With this container, you can use all Ansible command-line:

* ansible
* ansible-config
* ansible-console
* ansible-doc
* ansible-galaxy
* ansible-inventory
* ansible-playbook
* ansible-pull
* ansible-vault

The default one is "ansible-playbook".  

There is Python 2 (2.7.18) and 3 version (up to date at the moment the image is pushed).  
Even if Python 2 is deprecated, some old system need to be maintain. So I keep it for history.  
You can build and use the container behind a proxy.

## What contain the container

### Ansible addon

The default Ansible addon list for python3 in the container is in the [collection-requirements.yml](collection-requirements.yml) file.  

### Alpine package dependencies

The Alpine package dependencies are in the [Dockerfile.template](Dockerfile.template).  

### Python dependencies

The Python package dependencies are in the [Dockerfile.template](Dockerfile.template).  

## Quick reference

* Where to file issues: [https://gitlab.com/docker221/ansible/-/issues](https://gitlab.com/docker221/ansible/-/issues).  

## Supported tags for Ansible

Tags follow the versionning of Ansible. the latest tag for python 2 and 3 are:

* [9.3.0][def], [9.3.0-alpine3.19-py3.12.2][def]
* [2.3.3][def2], [2.3.3-alpine3.11-py2.7.18][def2]

For tag history, look [here](https://hub.docker.com/r/vandman/ansible/tags)

## How to use the container

Launch ansible-playbook with root-playbook.yml file and use of ssh connexion with root ssh key. It will deploy the PLD inventory.  

```shell
docker run --rm \
  --mount type=bind,src=${HOME}/.ssh,dst=/root/.ssh \
  --mount type=bind,src=/dir/to/ansible-playbook,dst=/srv/ansible \
  --workdir /srv/ansible \
  --env ANSIBLE_FORCE_COLOR=true \
  --env ANSIBLE_VERBOSITY=1
  vandman/ansible \
    -e @vars/main.yml \
    -i inventories/PLD/hosts \
    root-playbook.yml;
```

In the docker --env parameter, you can use all default Ansible [Environment Variables](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#environment-variables).  
The Playbook dir is mounted into **"/srv/ansible"**.  

You can find exemples on how to use this container in the gitlab project [lancement_ansible](https://gitlab.com/t8384/lancement_ansible) in the Makefile.  

[def]: python3/Dockerfile
[def2]: python2/Dockerfile
