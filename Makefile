#####################################################################
# This code can be used to build an ansible imabe and push it to
# Docker-hub or in a local/private registry
# The version of the image will follow the official ansible version.
#####################################################################

MAIN_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SHELL := /usr/bin/env bash

-include Makefile.env 

#####################################################################
# if for some reason, you need to increment your local ansible verion
# because you have chaged something in the Dockerfile or entrypoint,
# you can do so with the following variable
# If not set, it is not used
#####################################################################
LOCAL_ANSIBLE_VERSION ?=
#####################################################################
# This can be usefull if you are using a local registry like harbor
# If not set, it is not used
#####################################################################
# ex: my.syte.com/
LOCAL_REGISTRY_FQDN ?=
# ex: https://my.gitlab.com/registry/ca.crt?inline=false
LOCAL_REGISTRY_CRT_URL ?=
# ex: python/
REGISTRY_PYTHON_PATH ?=
# ex: ansible/     
REGISTRY_ANSIBLE_PATH ?= vandman/


#####################################################################
# If you use a proxy to get your package, you can use the following
# variables.
# If not set, it is not used
#####################################################################
# ex: my.nexus.com/
PROXY_FQDN ?=
# ex: for Nexus, the root path is "repository/"
PROXY_ROOT_PATH ?=
# ex: pypi-proxy/
PIP_PROXY_PATH ?=

# For old python déployement
ALPINE_VERSION_PYTHON2 ?= 3.11
PYTHON2_VERSION ?= 2.7.18
ANSIBLE_VERSION_PYTHON2 ?= 2.3.3
VERSION_PYTHON2_ALPINE ?= "$(PYTHON2_VERSION)-alpine$(ALPINE_VERSION_PYTHON2)"
# For new python déployement
ALPINE_VERSION_PYTHON3 ?= 3.18
PYTHON3_VERSION ?= 3.12.0
ANSIBLE_VERSION_PYTHON3 ?= 8.5.0
VERSION_PYTHON3_ALPINE ?= "$(PYTHON3_VERSION)-alpine$(ALPINE_VERSION_PYTHON3)"

export LOCAL_REGISTRY_FQDN
export LOCAL_REGISTRY_CRT_URL


--: ## ----  Pull python image from internet and store it on local registry ---------------------
pull-push-pyhton2: ## pull pyhton2 image from internet and store it into the local registry
pull-push-pyhton2:
	$(info --> Pull image from internet and store it into registry)
	docker pull python:$(VERSION_PYTHON2_ALPINE)
	if [ -z ${LOCAL_REGISTRY_FQDN+x} ]; \
	then \
	  echo "NO LOCAL REGISTRY configured"; \
	else \
	  echo "Local registry configured= '${LOCAL_REGISTRY_FQDN+x}'" &&\
	  docker image tag \
	   python:$(VERSION_PYTHON2_ALPINE) \
	   ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:$(VERSION_PYTHON2_ALPINE) && \
	  echo "push = docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:$(VERSION_PYTHON2_ALPINE)" &&\
	  docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:$(VERSION_PYTHON2_ALPINE) ; \
	fi

pull-push-pyhton3: ## pull pyhton3 image from internet and store it into the local registry
pull-push-pyhton3:
	$(info --> Pull image from internet and store it into registry)
	docker pull python:$(VERSION_PYTHON3_ALPINE)
	if [ -z ${LOCAL_REGISTRY_FQDN+x} ]; \
	then \
	  echo "NO LOCAL REGISTRY configured"; \
	else \
	  echo "Local registry configured= '${LOCAL_REGISTRY_FQDN+x}'" &&\
	  echo "Tag de l'image:" && \
	  echo "docker image tag python:$(VERSION_PYTHON3_ALPINE) ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:$(VERSION_PYTHON3_ALPINE)" && \
	  docker image tag \
	   python:$(VERSION_PYTHON3_ALPINE) \
	   ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:$(VERSION_PYTHON3_ALPINE) && \
	  echo "docker image tag python:$(VERSION_PYTHON3_ALPINE) ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:latest" && \
	  docker image tag \
	   python:$(VERSION_PYTHON3_ALPINE) \
	   ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:latest && \
	  echo "Push de l'image:" && \
	  echo "docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:$(VERSION_PYTHON3_ALPINE)" && \
	  docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:$(VERSION_PYTHON3_ALPINE) && \
	  echo "docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:latest" && \
	  docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_PYTHON_PATH}python:latest ; \
	fi

--: ## ----  Build Ansible with python image  ---------------------------------------------------
build-ansible-python2: ## Build ansible with python2 image.
build-ansible-python2:
	$(info --> Build python2 image)
	./update.sh python2
	echo "-t ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON2)$(LOCAL_ANSIBLE_VERSION)"; \
	cd python2; \
	docker build \
	 -t ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON2)$(LOCAL_ANSIBLE_VERSION) \
	 --build-arg ALPINE_VERSION=$(ALPINE_VERSION_PYTHON2) \
	 --build-arg PYTHON_VERSION=$(PYTHON2_VERSION) \
	 --build-arg PIP_PROXY=${PIP_PROXY} \
	 --build-arg ANSIBLE_VERSION=$(ANSIBLE_VERSION_PYTHON2) .
	docker image tag \
	 ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON2)$(LOCAL_ANSIBLE_VERSION) \
	 ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON2)$(LOCAL_ANSIBLE_VERSION)-alpine$(ALPINE_VERSION_PYTHON2)-py$(PYTHON2_VERSION)

build-ansible-python3: ## Build ansible with python3 image.
build-ansible-python3:
	$(info --> Build python3 image)
	./update.sh python3
	echo "-t ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION)"; \
	cd python3; \
	docker build \
	 -t ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION) \
	 --build-arg ALPINE_VERSION=$(ALPINE_VERSION_PYTHON3) \
	 --build-arg PYTHON_VERSION=$(PYTHON3_VERSION) \
	 --build-arg PIP_PROXY=${PIP_PROXY} \
	 --build-arg ANSIBLE_VERSION=$(ANSIBLE_VERSION_PYTHON3) .
	echo "Tag de l'image:"
	echo " \
	docker image tag \
	${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION) \
	${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION)-alpine$(ALPINE_VERSION_PYTHON3)-py$(PYTHON3_VERSION) \
	"
	docker image tag \
	${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION) \
	${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION)-alpine$(ALPINE_VERSION_PYTHON3)-py$(PYTHON3_VERSION)
	echo " \
	docker image tag \
	${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION) \
	${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:latest \
	"
	docker image tag \
	${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION) \
	${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:latest

--: ## ----  Push Ansible image to registry (local or on Docker Hub)  ---------------------------
push-ansible-python2: ## Push Ansible Python2 images and documentation to registry
push-ansible-python2:
	$(info --> Push images to registry)
	docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON2)$(LOCAL_ANSIBLE_VERSION)
	docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON2)$(LOCAL_ANSIBLE_VERSION)-alpine$(ALPINE_VERSION_PYTHON2)-py$(PYTHON2_VERSION)
	./dochub.sh

push-ansible-python3: ## Push Ansible Python3 images and documentation to registry
push-ansible-python3:
	$(info --> Push images to registry)
	docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION)
	docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:$(ANSIBLE_VERSION_PYTHON3)$(LOCAL_ANSIBLE_VERSION)-alpine$(ALPINE_VERSION_PYTHON3)-py$(PYTHON3_VERSION)
	docker push ${LOCAL_REGISTRY_FQDN}${REGISTRY_ANSIBLE_PATH}ansible:latest
	./dochub.sh


--: ## ----  Do all the job  --------------------------------------------------------------------
all2: ## Pull, build push an Ansible Python2 image to registry
all2:
	$(info --> Pull, build push an image to registry)
	if [ ! -z ${LOCAL_REGISTRY_FQDN+x} ]; \
	then \
	  echo "NO LOCAL REGISTRY configured"; \
	else \
	  $(MAKE) pull-push-pyhton2 ; \
	fi
	$(MAKE) build-ansible-python2
	$(MAKE) push-ansible-python2

all3: ## Pull, build push an Ansible Python3 image to registry
all3:
	$(info --> Pull, build push an image to registry)
	if [ -z ${LOCAL_REGISTRY_FQDN+x} ]; \
	then \
	  echo "NO LOCAL REGISTRY configured"; \
	else \
	  $(MAKE) pull-push-pyhton3 ; \
	fi
	$(MAKE) build-ansible-python3
	$(MAKE) push-ansible-python3

ifndef DEBUG
.SILENT:
endif

.PHONY: *

COLOR_SECTION = \033[33m
COLOR_TARGET  = \033[32m
COLOR_TEXT    = #\033[90m
COLOR_COMMENT = \033[90m
COLOR_RESET   = \033[0m
PADDING_SIZE  = 12
help:
	#$(MAKEFILE_LIST)
	grep -E '^[1-9a-zA-Z_-]+:.*?## .*$$' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf ($$1 ~ "--" ? "${COLOR_SECTION}" $$2 "${COLOR_RESET}\n" : "${COLOR_TARGET}%-${PADDING_SIZE}s ${COLOR_RESET}${COLOR_TEXT}%s${COLOR_RESET}\n", $$1,  $$2)}'
	echo -e "${COLOR_SECTION}------------------------------------------------------------------------------------------${COLOR_RESET}"
	echo
.DEFAULT_GOAL := help
