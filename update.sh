#!/usr/bin/env bash
###############################################
## Script to create Dockerfile from Template ##
###############################################

source Makefile.env

python_version=$1

rm -rf ${python_version}
mkdir ${python_version}
cp Dockerfile.template ${python_version}/Dockerfile
cp docker-entrypoint ${python_version}/ && chmod 755 ${python_version}/docker-entrypoint
cd ${python_version}

if [ "${python_version}" == "python2" ];
then
    # Update documentation with latest version:
    sed -i "s/^.*\[def2]\$/* [${ANSIBLE_VERSION_PYTHON2}][def2], [${ANSIBLE_VERSION_PYTHON2}-alpine${ALPINE_VERSION_PYTHON2}-py${PYTHON2_VERSION}][def2]/" ../README.md
    sed -i "s/^.*\[def2]\$/* [${ANSIBLE_VERSION_PYTHON2}][def2], [${ANSIBLE_VERSION_PYTHON2}-alpine${ALPINE_VERSION_PYTHON2}-py${PYTHON2_VERSION}][def2]/" ../LISEZMOI.md
    # Modification for Python2
    # Do not use later version of the following package, it will not work with python 2
    sed -i 's/python3-dev/python-dev/' Dockerfile
    sed -i 's/COPY collection-requirements.yml.*$//' Dockerfile
    sed -i 's/PyMySQL/PyMySQL==0.9.3/' Dockerfile
    sed -i 's/cryptography/cryptography==3.3.2/' Dockerfile
    sed -i 's/ansible-galaxy collection install.*$/\\/' Dockerfile
    sed -i 's/apk add --no-cache postgresql-libs git xmlstarlet openssh-client sshpass make bash openssl curl file grep sed docker-cli/apk add --no-cache postgresql-libs git xmlstarlet openssh-client sshpass make bash openssl curl file grep sed docker-cli py-simplejson /g' Dockerfile
    sed -i 's/pip install --no-cache docker docker-compose/pip install --no-cache simplejson/g' Dockerfile
fi

if [ "${python_version}" == "python3" ];
then
    # Update documentation with latest version:
    sed -i "s/^.*\[def\]\$/* [${ANSIBLE_VERSION_PYTHON3}][def], [${ANSIBLE_VERSION_PYTHON3}-alpine${ALPINE_VERSION_PYTHON3}-py${PYTHON3_VERSION}][def]/" ../README.md
    sed -i "s/^.*\[def\]\$/* [${ANSIBLE_VERSION_PYTHON3}][def], [${ANSIBLE_VERSION_PYTHON3}-alpine${ALPINE_VERSION_PYTHON3}-py${PYTHON3_VERSION}][def]/" ../LISEZMOI.md
    cp ../collection-requirements.yml ./
fi

# Local Docker registry configuration
# It is used by ansible to download image if needed.
if [ ! -z ${LOCAL_REGISTRY_FQDN+x} ];
then
    echo "No local registry"; \
else
    sed -i "/.*\/usr\/local\/bin.*/i ADD ${LOCAL_REGISTRY_CRT_URL} /etc/docker/certs.d/${REGISTRY_HOST}/ca.crt" Dockerfile
    sed -i "/.*\/usr\/local\/bin.*/i ENV REGISTRY_HOST=${LOCAL_REGISTRY_FQDN}" Dockerfile
fi
